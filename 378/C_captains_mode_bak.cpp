#include<iostream>
#include<vector>
using namespace std;

typedef pair<char, int> PCI;
int n, m, ans[21][101], sum = 0;
vector<int> s;
int valid[21][201];
PCI op[21];

int solve(int opn, int start) {
  int res = 0;
  if(valid[opn][start] == 1) return ans[opn][start];
  else if(opn == m && start <= n) { ans[opn][start] = 0; valid[opn][start] = 1; return 0; }
  else if(start >= n) { valid[opn][start] = -1; return 0; }
  if(op[opn].first == 'p') {
    res = solve(opn+1, start+1);
    if(valid[opn+1][start+1] == 1) res += (op[opn].second == 1 ? s[start] : -s[start]);
    else { valid[opn][start] = -1; return 0; }
  } else {
    int res3[] = { 0, 0, 0 };
    res3[0] = solve(opn+1, start+1), res3[1] = solve(opn+1, start);
    res3[2] = solve(opn, start+1) };
    bool v[] = { valid[opn+1][start+1], valid[opn+1][start], valid[opn][start+1] };
    vector<int> candidate;
    for(int i = 0; i < 3; i ++) if(v[i]) candidate.push_back(res3[i]);
    if(candidate.size() <= 0) { valid[opn][start] = -1; return 0; }
    else if(op[opn].second == 1) res = *max_element(candidate.begin(), candidate.end());
    else res = *min_element(candidate.begin(), candidate.end());
  }
  ans[opn][start] = res;
  valid[opn][start] = 1;
  return res;
}

int main() {
  cin >> n;
  int temp;
  for(int i = 0; i < n; i ++) { cin >> temp; s.push_back(temp); }
  sort(s.begin(), s.end()); reverse(s.begin(), s.end());
  cin >> m;
  for(int i = 1; i <= m; i ++) cin >> op[i].first >> op[i].second;
  for(int i = 0; i <= m; i ++) for(int j = 0; j <= n; j ++) ans[i][j] = 0;
  cout << solve(1, 0) << endl;
  return 0;
}
