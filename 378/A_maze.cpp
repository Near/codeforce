#include<iostream>
#include<algorithm>
#include<vector>
#include<set>
#include<queue>
#include<cstring>
#include<cstdlib>
#include<map>
using namespace std;

typedef pair<int, int> PII;
const int MAXN = 500, MAXM = 500;
int n, m, k, dx[] = {1, 0, -1, 0}, dy[] = {0, 1, 0, -1};
char ch[MAXN+2][MAXM+2];
bool v[MAXN+2][MAXM+2];

void dfs(int x, int y) {
  if(x < 0 || x >= n || y < 0 || y >= m || ch[x][y] != '.' || v[x][y]) return;
  v[x][y] = true;
  for(int i = 0; i < 4; i ++) dfs(x+dx[i], y+dy[i]);
  if(k) { ch[x][y] = 'X'; k --; }
}

int main() {
  cin >> n >> m >> k;
  for(int i = 0; i < n; i ++) cin >> ch[i];
  for(int i = 0; i < n && k > 0; i ++)
    for(int j = 0; j < m && k > 0; j ++) {
      dfs(i, j);
    }
  for(int i = 0; i < n; i ++) cout << ch[i] << endl;
  return 0;
}
