#include<iostream>
#include<algorithm>
#include<vector>
#include<queue>
#include<cstring>
#include<cstdlib>
#include<map>
using namespace std;

typedef pair<int, int> PII;
const int MAXN = 100000, MAXM = 100000;
int n, m, s, c[MAXN+1], res[MAXM+1];
PII a[MAXM+1], b[MAXN+1];

bool solve(int d) {
  priority_queue<PII, vector<PII>, greater<PII> > pq;
  int i, j, cur = 0;
  for(i = j = 1; i <= m; i += d) {
    for(; j <= n && b[j].first >= a[i].first; j ++) pq.push(PII(c[b[j].second], b[j].second));
    if(pq.empty()) return false;
    PII temp = pq.top();
    cur += temp.first;
    if(cur > s) return false;
    for(int k = 0; k < d && i + k <= m; k ++) res[a[i + k].second] = temp.second;
    pq.pop();
  }
  return true;
}

int main() {
  cin >> n >> m >> s;
  for(int i = 1; i <= m; i ++) { cin >> a[i].first; a[i].second = i; }
  for(int i = 1; i <= n; i ++) { cin >> b[i].first; b[i].second = i; }
  for(int i = 1; i <= n; i ++) cin >> c[i];
  sort(a+1, a+m+1); reverse(a+1, a+m+1);
  sort(b+1, b+n+1); reverse(b+1, b+n+1);
  if(!solve(m)) { cout << "NO" << endl; return 0; }
  int l = 1, r = m;
  while(l < r) {
    int mid = (l+r)/2;
    if(solve(mid)) r = mid;
    else l = mid + 1;
  }
  solve(r);
  cout << "YES" << endl;
  for(int i = 1; i <= m; i ++) cout << res[i] << " ";
  cout << endl;
  return 0;
}
