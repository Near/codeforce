#include<iostream>
#include<vector>
#include<queue>
#include<map>
#include<cstdlib>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

const int MAXK = 100000, MAXT = 100000;
long long t, k, a[MAXT], b[MAXT], ans[MAXK + 1];
int mod = 1000000007;

void init(int k) {
  ans[0] = 0;
  for(int i = 0; i < k; i ++) ans[i] = 1;
  for(int i = k; i <= MAXK; i ++) {
    ans[i] = ans[i - 1] + ans[i - k];
    ans[i] %= mod;
  }

  for(int i = 1; i <= MAXK; i ++) {
    ans[i] += ans[i - 1];
    ans[i] %= mod;
  }
}

int main() {
  cin >> t >> k;
  init(k);
  for(int i = 0; i < t; i ++) {
    cin >> a[i] >> b[i];
    cout << (ans[b[i]] - ans[a[i] - 1] + mod) % mod << endl;
  }
}
