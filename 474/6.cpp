#include<iostream>
#include<vector>
#include<queue>
#include<map>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<cstdio>
#include<cmath>
using namespace std;

const int MAXN = 1e5 + 19;
long long MIN[21][MAXN+1], GCD[21][MAXN+1], n, t;
pair<long long, long long> A[MAXN+1];

int gcd(int a, int b) {
  return b == 0 ? a : gcd(b, a % b);
}

int main() {
  cin >> n;
  for(int i = 1; i <= n; i ++) {
    cin >> t;
    MIN[0][i] = GCD[0][i] = t;
    A[i] = make_pair(t, i);
  }

  for(int i = 1; i < 21; i ++) {
    for(int j = 1; j <= n - (1 << i) + 1; j ++) {
      MIN[i][j] = min(MIN[i-1][j], MIN[i-1][j + (1 << (i - 1))]);
      GCD[i][j] = gcd(GCD[i-1][j], GCD[i-1][j + (1 << (i - 1))]);
    }
  }
  sort(A + 1, A + n + 1);

  cin >> t;
  long long l, r, k;
  for(int i = 1; i <= t; i ++) {
    cin >> l >> r;
    for(k = 0; 1 << (k + 1) <= r - l + 1; k ++);
    long long g = gcd(GCD[k][l], GCD[k][r - (1 << k) + 1]);
    long long m = min(MIN[k][l], GCD[k][r - (1 << k) + 1]);
    if(g != m) cout << r - l + 1 << endl;
    else {
      int cnt = upper_bound(A+1,A+n+1,make_pair(g, r)) - lower_bound(A+1,A+n+1,make_pair(g, l));
      cout << r - l + 1 - cnt << endl;
    }
  }
}
