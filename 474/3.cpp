#include<iostream>
#include<vector>
#include<queue>
#include<map>
#include<cstdlib>
#include<string>
#include<climits>
#include<algorithm>
using namespace std;

const int MAXN = 100000, MAXW = 100000, MAXK = 10;
long long n, x[MAXN], y[MAXN], a[MAXN], b[MAXN];
long long x2[MAXN], y2[MAXN], a2[MAXN], b2[MAXN];

void disrotate(int i) {
  x2[i] = x[i];
  y2[i] = y[i];
}

void rotate(int i, int k) {
  x2[i] = x[i];
  y2[i] = y[i];
  for(int j = 0; j < k; j ++) {
    int xtemp = a[i] + b[i] - y2[i];
    int ytemp = b[i] + x2[i] - a[i];
    x2[i] = xtemp;
    y2[i] = ytemp;
  }
}

long long dis(long long dx, long long dy) {
  return dx * dx + dy * dy;
}

bool isCompact(int k) {
  bool ret = true;
  long long d = LLONG_MAX, dd = 0;
  for(int i = 0; i < 4; i ++) {
    for(int j = 0; j < 4; j ++) {
      if(j == i) continue;
      d = min(d, dis(x2[4 * k + i] - x2[4 * k + j], y2[4 * k + i] - y2[4 * k + j]));
      dd = max(dd, dis(x2[4 * k + i] - x2[4 * k + j], y2[4 * k + i] - y2[4 * k + j]));
    }
  }
  return d != 0 && dd != 0 && dd == 2 * d;
}

int canBeCompact(int k) {
  for(int i = 0; i < 16; i ++) {
    for(int k1 = 0; k1 <= min(4, i); k1 ++) {
      rotate(k * 4, k1);
      for(int k2 = 0; k2 <= min(4, i - k1); k2 ++) {
        rotate(k * 4 + 1, k2);
        for(int k3 = 0; k3 <= min(4, i - k1 - k2); k3 ++) {
          rotate(k * 4 + 2, k3);
          for(int k4 = 0; k4 <= min(4, i - k1 - k2 - k3); k4 ++) {
            rotate(k * 4 + 3, k4);
            //cout << k1 << " " << k2 << " " << k3 << " " << k4 << endl;
            if(isCompact(k)) {
              return i;
            }
          }
          disrotate(k * 4 + 3);
        }
        disrotate(k * 4 + 2);
      }
      disrotate(k * 4 + 1);
    }
    disrotate(k * 4 + 0);
  }
  return -1;
}

int main() {
  cin >> n;
  for(int i = 0; i < n; i ++) {
    for(int j = 0; j < 4; j ++) {
      int m = 4 * i + j;
      cin >> x[m] >> y[m] >> a[m] >> b[m];
      //cout << x[m] << y[m] << endl;
    }
    cout << canBeCompact(i) << endl;
  }
  return 0;
}
