#include<iostream>
#include<vector>
#include<queue>
#include<map>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<cmath>
using namespace std;

const int MAXN = 100001, MAXD = 1000000001;
long long n, d, h[MAXN], ans[MAXN], nex[MAXN];

int main() {
  cin >> n >> d;
  for(int i = 1; i <= n; i ++) {
    cin >> h[i];
    nex[i] = -1;
    ans[i] = 1;
  }

  int max = 1, start = n;
  for(int i = n; i >= 1; i --) {
    for(int j = i + 1; j <= min((long long)i + 700, n); j ++) {
      if(abs(h[j] - h[i]) >= d && ans[j] + 1 > ans[i]) {
        nex[i] = j;
        ans[i] = ans[j] + 1;
      }
    }
    if(ans[i] > max) {
      max = ans[i];
      start = i;
    }
  }
  cout << max << endl;
  int i = start;
  while(i != -1) {
    cout << i << " ";
    i = nex[i];
  }
  return 0;
}
