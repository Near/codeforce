#include<iostream>
#include<vector>
#include<queue>
#include<map>
#include<cstdlib>
#include<string>
#include<algorithm>
using namespace std;

const int MAXN = 100000, MAXM = 10000;
int n = 10000, m = 10000, a, b;

const char* key[3] = {
  "qwertyuiop",
  "asdfghjkl;",
  "zxcvbnm,./"
};

int main() {
  char c;
  int offset = 0;
  cin >> c;
  if (c == 'R') offset = -1;
  else offset = 1;
  string s;
  cin >> s;

  for(int i = 0; i < s.length(); i ++) {
    for(int j = 0; j < 3; j ++) {
      for(int k = 0; k < 10; k ++) {
        if(key[j][k] == s[i]) {
          s[i] = key[j][k+offset];
          break;
        }
      }
    }
  }
  cout << s << endl;

  return 0;
}
