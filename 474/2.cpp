#include<iostream>
#include<vector>
#include<queue>
#include<map>
#include<cstdlib>
#include<string>
#include<algorithm>
using namespace std;

const int MAXN = 100000, MAXM = 100000;
int a[MAXN], q[MAXM];
int m, n;

int biSearch(int b) {
  int l = 1, r = n;
  while(l < r) {
    int m = (l + r) / 2;
    if(a[m] == b) return m;
    else if(a[m] > b) r = m;
    else l = m+1;
  }
  return l;
}

int main() {
  cin >> n;
  a[0] = 0;
  for(int i = 1; i <= n; i ++) {
    cin >> a[i];
    a[i] = a[i] + a[i-1];
  }
  cin >> m;
  for(int j = 0; j < m; j ++) {
    cin >> q[j];
    cout << biSearch(q[j]) << endl;
  }

  return 0;
}
