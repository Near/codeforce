n=input()
a=[map(int,raw_input().split()) for i in range(n)]
def check(i):
  for j in range(n):
    if (i!=j)and(all(x in a[i][1:] for x in a[j][1:])):return 'NO'
  return 'YES'
print '\n'.join(map(check,range(n)))
