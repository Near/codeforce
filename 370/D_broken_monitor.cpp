#include<iostream>
#include<vector>
using namespace std;
const int MAXSIZE = 2001;
int n, m, size = 0, count = 0, sumX[MAXSIZE][MAXSIZE], sumY[MAXSIZE][MAXSIZE];
char monitor[MAXSIZE][MAXSIZE];

void output() {
  for(int i = 1; i <= n; i ++) {
    for(int j = 1; j <= m; j ++) {
      cout << monitor[i][j];
    }
    cout << endl;
  }
}

void paint(int x, int y) {
  for(int i = 0; i <= size; i ++) {
    if(monitor[x][y+i] != 'w') monitor[x][y+i] = '+';
    if(monitor[x+size][y+i] != 'w') monitor[x+size][y+i] = '+';
    if(monitor[x+i][y] != 'w') monitor[x+i][y] = '+';
    if(monitor[x+i][y+size] != 'w') monitor[x+i][y+size] = '+';
  }
}

bool judge(int i, int j) {
  int left = count - (sumX[i][j+size] - sumX[i][j]) - (sumX[i+size][j+size] - sumX[i+size][j])
    - (sumY[i+size][j] - sumY[i-1][j]) - (sumY[i+size-1][j+size] - sumY[i][j+size]);
  return left == 0;
}

int main() {
  cin >> n >> m;
  int left = m, right = 0, top = n, bottom = 0;
  for(int i = 1; i <= n; i ++) cin >> (monitor[i] + 1);
  for(int i = 1; i <= n; i ++) {
    for(int j = 1; j <= m; j ++)
      if(monitor[i][j] == 'w') {
        left = min(j, left), right = max(j, right), top = min(i, top), bottom = max(i, bottom);
        count ++;
      }
    for(int j = 1; j <= m; j ++) {
      sumX[i][j] = sumX[i][j-1] + (monitor[i][j] == 'w');
      sumY[i][j] = sumY[i-1][j] + (monitor[i][j] == 'w');
    }
  }
  size = max(right - left, bottom - top);
  if(size == 0) { output(); return 0; }
  for(int i = 1; i <= n - size; i ++) {
    for(int j = 1; j <= m - size; j ++) {
      if(judge(i, j)) {
        paint(i, j);
        output();
        return 0;
      }
    }
  }
  cout << -1 << endl;
  return 0;
}
