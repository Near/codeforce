#include<iostream>
#include<cstring>
#include<set>
using namespace std;
const int MAXSIZE = 101;
int n, m[MAXSIZE], input;
bool win[MAXSIZE], notIn[MAXSIZE];
set<int> cards[MAXSIZE], players[2], finish;

int main() {
  cin >> n;
  for(int i = 0; i < n; i ++) {
    cin >> m[i];
    win[i] = true;
    notIn[i] = false;
    players[0].insert(i);
    for(int j = 0; j < m[i]; j ++) {
      cin >> input;
      cards[i].insert(input);
    }
  }
  int current = 0;
  while(!players[current].empty()) {
    players[1-current].clear();
    memset(notIn, 0, sizeof(bool)*MAXSIZE);
    finish.clear();
    int cur_p = *(players[current].begin());
    for(set<int>::iterator num = cards[cur_p].begin(); num != cards[cur_p].end(); num ++) {
      int toErase = *num;
      for(set<int>::iterator p = players[current].begin(); p != players[current].end(); p ++) {
        if(notIn[*p]) continue;
        int temp = cards[*p].erase(toErase);
        if(temp == 0) { players[1-current].insert(*p); notIn[*p] = true; }
        else if(cards[*p].size() == 0) finish.insert(*p);
      }
      if(!finish.empty()) break;
    }
    for(set<int>::iterator p = players[current].begin(); p != players[current].end(); p ++) {
      if(notIn[*p]) continue;
      win[*p] = false;
    }
    if(finish.size() == 1)
      for(set<int>::iterator p = finish.begin(); p != finish.end(); p ++) {
        win[*p] = true;
      }
    current = 1 - current;
  }
  for(int i = 0; i < n; i ++)
    cout << (win[i] ? "YES" : "NO") << endl;
  return 0;
}
