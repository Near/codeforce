import math
r1,c1,r2,c2 = map(int, raw_input().split())
print (r1 != r2) + (c1 != c2),
if (r1 + c1 - r2 - c2) % 2 != 0:
  print 0,
else:
  print (c2 - r2 != c1 - r1) + (c2 + r2 != c1 + r1),
print max(abs(r1 - r2), abs(c1 - c2))
