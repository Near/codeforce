#include<iostream>
#include<vector>
#include<cstring>
using namespace std;

int main() {
  int n;
  vector<int> p;
  p.push_back(0);

  cin >> n;
  int a[n+1], status[6][n+1];
  memset(status, 0, sizeof(int)*6*(n+1));
  a[0] = 0;
  for(int i = 0; i < n; i ++) {
    cin >> a[i+1];
    if(a[i+1]) p.push_back(i+1);
  }
  if(p.size() == 1) {
    cout << n/2 << endl;
    for(int i = 0; i < n/2; i ++) cout << i+1 << " " << i+1 << " ";
    if(n%2) cout << n/2 << endl;
    else cout << endl;
    return 0;
  } else if(a[1] > 1 || a[2] > 1) {
    cout << -1 << endl;
    return 0;
  }
  status[5][0] = 1;
  for(int i = 1; i < p.size(); i ++) {
    if(a[p[i]] < a[p[i-1]]) {
      cout << -1 << endl;
      return 0;
    } else if(a[p[i]] == a[p[i-1]]) {
      for(int j = p[i-1]+1; j < p[i]; j ++) a[j] = a[p[i]];
      int dis = p[i] - p[i-1];
      for(int y = 1; y <= 5; y ++) {
        if(y + dis > 5) continue;
        else if(status[y][p[i-1]]) status[y+dis][p[i]] |= 1<<y;
      }
    } else {
      for(int x = 0; x < 5; x ++) {
        for(int y = 1; y <= 5; y ++) {
          int l = p[i] - p[i-1] - x - y;
          if(l < 2*(a[p[i]] - a[p[i-1]] - 1) || l > 5*(a[p[i]] - a[p[i-1]] - 1)) continue;
          for(int j = 1; j <= 5; j ++) {
            if(x + j < 2 || x + j > 5) continue;
            else if(status[j][p[i-1]]) status[y][p[i]] |= 1 << (x*6+j);
          }
        }
      }
    }
  }

  int ans = 0, y, l;
  for(int x = 1; x <= 5; x ++) {
    if(status[x][p.back()] == 0) continue;
    y = n - p.back();
    if(x == 1 && y < 1) continue;
    else if(x == 5 && y == 1) continue;
    if(x == 1) y-= 1;
    y = a[p.back()] + y/2;
    if(y > ans) ans = y, l = x;
  }
  if(ans == 0) {
    cout << -1 << endl;
    return 0;
  }
  int x = p.back() + 1;
  if(l == 1) a[x] = a[x-1], x ++;
  while(x < n) a[x] = a[x+1] = a[x-1]+1, x+=2;
  if(x == n) a[x] = a[x-1] , x++;

  for(int i = p.size() - 1; i > 0; i --) {
    if(a[p[i]] == a[p[i-1]]) {
      l -= p[i] - p[i-1];
      continue;
    } else {
      for(int j = p[i] - l + 1; j < p[i]; j ++) a[j] = a[p[i]];
      for(int s = 1; s < 30; s ++) {
        if(status[l][p[i]] & 1 << s) {
          int x = s / 6, dist = p[i] - l - p[i-1] - x + 1;
          for(int j = p[i-1] + 1; j <= p[i-1] + x; j ++) a[j] = a[p[i-1]];
          for(int j = p[i-1] + x + 1; j < p[i] - l + 1; j ++)
            a[j] = a[p[i-1]] + 1 + (j - p[i-1] - x) * (a[p[i]] - a[p[i-1]] - 1) / dist;
          l = s%6;
          break;
        }
      }
    }
  }
  cout << ans << endl;
  for(int i = 1; i <= n; i ++)
    cout << a[i] << " ";
  cout << endl;
  return 0;
}
