#include<iostream>
#include<string.h>
#include<vector>
#include<set>
#include<map>
#include<queue>
#include<algorithm>
#include<cstdlib>
using namespace std;

const int MAXN = 1000, MAXM = 10000;
int n, card[MAXN];

int main() {
  cin >> n;
  for(int i = 0; i < n; i ++) {
    cin >> card[i];
  }
  int start = 0, end = n-1, s[2] = {0, 0}, c = 0;
  while(start <= end) {
    s[c] += max(card[start], card[end]);
    c = 1-c;
    if(card[start] > card[end]) start ++;
    else end --;
  }
  cout << s[0] << " " << s[1] << endl;
  return 0;
}
