#include<iostream>
#include<string.h>
#include<vector>
#include<set>
#include<map>
#include<queue>
#include<algorithm>
#include<cstdlib>
using namespace std;

const int MAXM = 100000;
int m, b[MAXM], c[5001];
vector<int> res;

int main() {
  int maxb = 0, ans = 0;
  cin >> m;
  for(int i = 0; i < m; i ++) {
    cin >> b[i];
    maxb = max(maxb, b[i]);
    c[b[i]] += 1;
  }
  ans += 1;
  res.push_back(maxb);
  for(int i = maxb-1; i >= 1; i --) {
    if(c[i] >= 2) { res.insert(res.begin(), i); res.push_back(i); }
    else if(c[i] == 1) res.insert(res.begin(), i);
  }
  cout << res.size() << endl;
  for(auto i : res) cout << i << " ";
  cout << endl;
  return 0;
}
