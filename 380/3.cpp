#include<iostream>
#include<string.h>
#include<vector>
#include<set>
#include<map>
#include<queue>
#include<algorithm>
#include<cstdlib>
using namespace std;

typedef pair<int, int> pii;
const int MAXN = 100000, MAXM = 100000;
int n, m, t, l, c, len;
pii stage[MAXM + 1];
int type[MAXM + 1], value[MAXM + 1];
vector<long long> lens;
map<long long, int> ltos;
map<long long, int> ans;

int bin_search(int l) {
  int lo = 0, hi = lens.size() - 1;
  while(lo < hi) {
    int mid = (lo + hi + 1) / 2;
    if(lens[mid] == l) return l;
    else if(lens[mid] < l) lo = mid;
    else hi = mid - 1;
  }
  return lens[lo];
}

int main() {
  cin >> m;
  stage[0] = pii(0, 0);
  for(int i = 1; i <= m; i ++) {
    cin >> t >> l;
    ltos[len] = i;
    type[i] = t;
    lens.push_back(len);
    if(t == 1) {
      stage[i] = pii(len, len + 1);
      value[i] = l;
      len ++;
    } else {
      cin >> c;
      stage[i] = pii(len, len + c * l);
      value[i] = l;
      len += c * l;
    }
  }
  cin >> n;
  for(int i = 0; i < n; i ++) {
    cin >> l;
    l --;
    int ls = bin_search(l), st = ltos[ls], origL = l;
    while(type[st] != 1) {
      l = (l - ls) % value[st];
      if(ans.find(l) != ans.end()) st = ans[l]; 
      else { ls = bin_search(l), st = ltos[ls]; }
    }
    ans[origL] = st;
    cout << value[st] << " ";
  }
  cout << endl;
  return 0;
}
