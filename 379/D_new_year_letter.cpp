#include<iostream>
#include<cstdlib>
using namespace std;
int k, x, n, m;

long long count(int t, long long ac, long long bc, int as, int ae, int bs, int be) {
  if(t == 2) return bc;
  else return count(t-1, bc, ac+bc+(ae*bs), bs, be, as, be);
}

string make(int l, int c, int s, int e) {
  string r;
  r.resize(l);
  int i = 0;
  if(s) r[i++] = 'C';
  if(e) r[--l] = 'A';
  while(c --) { r[i++] = 'A'; r[i++] = 'C'; }
  while(i < l) r[i++] = 'X';
  return r;
}

void solve(long long ac, long long bc, int as, int ae, int bs, int be) {
  if(ac * 2 + as + ae > n) return;
  if(bc * 2 + bs + be > m) return;
  if(count(k, ac, bc, as, ae, bs, be) == x) {
    cout << make(n, ac, as, ae) << "\n" << make(m, bc, bs, be) << endl;
    exit(0);
  }
}

int main() {
  cin >> k >> x >> n >> m;
  for(int i = 0; 2*i <= n; i ++) {
    for(int j = 0; 2*j <= m; j ++) {
      for(int a = 0; a < 16; a ++) {
        solve(i, j, 1&a, 1&(a>>1), 1&(a>>2), 1&(a>>3));
      }
    }
  }
  cout << "Happy new year!" << endl;
  return 0;
}
