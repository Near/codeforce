#include<iostream>
#include<vector>
using namespace std;
int n, a[301], l = -1, r = -1;
vector<char> op;

void solve(bool tor) {
  int start = tor ? l : r, end = l + r - start, step = tor ? 1 : -1;
  for(int i = start+step; true; i += step) {
    op.push_back(tor ? 'R' : 'L');
    if(a[i] > 0) {
      op.push_back('P');
      a[i] -= 1;
      if(a[start] == 0) {
        start = i;
      }
    }
    if(i == end) break;
  }
  if(tor) l = start;
  else r = start;
}

int main() {
  cin >> n;
  for(int i = 0; i < n; i ++) {
    cin >> a[i];
    if(l == -1 && a[i] > 0) l = i;
    if(a[i] > 0) r = max(r, i);
  }
  for(int i = 0; i < l; i ++) op.push_back('R');
  op.push_back('P');
  a[l] -= 1;
  bool tor = true;
  while(l != r) {
    solve(tor);
    tor = !tor;
  }
  if(a[l] != 0) {
    char d;
    if(l == 0) d = 'R';
    else d = 'L';
    for(int i = 0; i < a[l]; i ++) {
      op.push_back(d);
      op.push_back('L' + 'R' - d);
      op.push_back('P');
    }
  }
  for(auto c : op) cout << c;
  cout << endl;
  return 0;
}
