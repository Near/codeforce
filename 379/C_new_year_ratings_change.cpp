#include<iostream>
#include<set>
#include<algorithm>
#include<map>
using namespace std;
set<int> nums;
map<int, int> c;
typedef pair<int, int> pii;
int n, ans[300000];
pii a[300000];

int main() {
  cin >> n;
  for(int i = 0; i < n; i ++) {
    cin >> a[i].first;
    nums.insert(a[i].first);
    c[a[i].first] ++;
    a[i].second = i;
  }
  sort(a, a+n);
  int cur = 0;
  for(set<int>::iterator iter = nums.begin(); iter != nums.end(); iter ++) {
    int start = *iter, sum = c[start], end = start + sum - 1;
    while(++ iter != nums.end() && *iter <= end) {
      sum += c[*iter];
      end = start + sum - 1;
    }
    iter --;
    for(int i = 0; i < sum; i ++) a[i + cur].first = i + start;
    cur += sum;
  }
  for(int i = 0; i < n; i ++) ans[a[i].second] = a[i].first;
  for(int i = 0; i < n; i ++) cout << ans[i] << " ";
  cout << endl;
  return 0;
}
