#include<iostream>
using namespace std;

int main() {
  int ans = 0, a, b, res = 0;
  cin >> a >> b;
  while(a > 0) {
    ans += a;
    a += res;
    res = a%b;
    a /= b;
  }
  cout << ans << endl;
  return 0;
}
