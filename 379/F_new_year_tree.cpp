#include <stdio.h>
#include <algorithm>
using namespace std;
int deep[1000010];
int f[1000010][20];
void add(int x, int fa){
    deep[x] = deep[fa] + 1;
    f[x][0] = fa;
    for (int i = 1; i < 20; i++) f[x][i] = f[f[x][i - 1]][i - 1];
}
int lca(int x, int y){
    int i;
    if (deep[x] < deep[y]) swap(x, y);
    for (i = 0; i < 20; i++) if ((deep[x] - deep[y]) & (1<<i)) x = f[x][i];
    for (i = 19; i >= 0; i--) if (f[x][i] != f[y][i]) x = f[x][i], y = f[y][i];
    return f[x][0];
}
int query(int x, int y){
    return deep[x] + deep[y] - (deep[lca(x, y)]<<1);
}
int main(){
    int n = 3, m, x, p = 1, q = 2, ans = 2;
    deep[1] = deep[2] = deep[3] = 1;
    scanf("%d", &m);
    while (m--){
            scanf("%d", &x); x--;
            add(++n, x);
            add(++n, x);
            if (query(p, n) > ans) ans++, q = n;
            else if (query(q, n) > ans) ans++, p = n;
            printf("%d\n", ans);
        }
    return 0;
}
