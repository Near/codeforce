#include<cstdio>
#include<iostream>
using namespace std;

int v[301][301];
double s[301];
const int D = 2500;
int main() {
  int n, k;
  cin >> n >> k;
  for(int i = 0; i < n; i ++)
    for(int j = 0; j < k+1; j ++)
      cin >> v[i][j];
  for(int x = 0; x < k; x ++) {
    for(int d = 0; d < D+1; d ++) {
      int ymax = 0;
      for(int i = 0; i < n; i ++) {
        int ycur = v[i][x] * (D-d) + v[i][x+1] * d;
        int h = ycur > ymax ? ycur - ymax : 0;
        ymax += h;
        s[i] += h;
        if(d != 0 && d != D) s[i] += h;
      }
    }
  }
  for(int i = 0; i < n; i ++) printf("%.9f\n", s[i]/D/5000);
  return 0;
}
