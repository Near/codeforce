#include<iostream>
#include<algorithm>
#include<vector>
#include<queue>
#include<cstring>
#include<cstdlib>
#include<map>
using namespace std;

typedef pair<int, int> PII;
const int MAXN = 10000, MAXM = 10000;
int n, m;

int main() {
  cin >> n >> m;
  int left = min(n, m), right = max(n, m);
  if(left == right) cout << "0 6 0" << endl;
  else {
    int a, b, draw;
    draw = (left + right + 1) % 2;
    a = (left + right - 1) / 2;
    b = 6 - a - draw;
    if(n == left) cout << a << " " << draw << " " << b << endl;
    else cout << b << " " << draw << " " << a << endl;
  }
  return 0;
}
