#include<iostream>
#include<algorithm>
#include<vector>
#include<queue>
#include<cstring>
#include<cstdlib>
#include<set>
#include<map>
using namespace std;

typedef pair<int, int> PII;
const int MAXN = 100000, MAXM = 10000;
int n, a, b;
map<int, int> h;
vector<int> v;
bool w[2*MAXN+1];

int main() {
  cin >> n;
  for(int i = 1; i <= n; i ++) {
    if(2*i <= n) {
      w[i] = w[i+n] = true;
    }
    cin >> a >> b;
    h.insert(PII(a, i));
    h.insert(PII(b, i+n));
    v.push_back(a);
    v.push_back(b);
  }
  sort(v.begin(), v.end());
  int count = 0;
  for(auto e : v) {
    count ++;
    if(count > n) break;
    w[h[e]] = true;
  }
  for(int i = 1; i <= n; i ++) cout << w[i];
  cout << endl;
  for(int i = 1; i <= n; i ++) cout << w[i+n];
  cout << endl;
  return 0;
}
