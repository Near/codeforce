#include<iostream>
#include<vector>
#include<algorithm>
#include<cstring>
#include<string>
#include<set>
using namespace std;

#define FOR(x, m) for(int x = 0; x < m; x ++)
#define MCR(x) (memset(x, 0, sizeof(x)))
typedef pair<int, int> PII;

const int MAXN = 1001;
int n, m, a[MAXN][MAXN][2], ans = 0, startI, startJ;
vector<PII> D;
set<PII> cycle;

int solve(int i, int j, int next) {
  if(a[i][j][1] > 0) return a[i][j][1];
  if(a[i][j][0] == 1 && cycle.find(PII(i, j)) != cycle.end()) {
    if(a[i][j][1] == 0) return n*m;
    else return a[i][j][1];
  }
  else if(a[i][j][0] == 1) cycle.insert(PII(i, j));
  int ans = 0, newNext = next == 4 ? 1 : next + 1;
  if(i >= 2 && a[i-1][j][0] == next) ans = max(ans, solve(i-1, j, newNext));
  if(i < n && a[i+1][j][0] == next) ans = max(ans, solve(i+1, j, newNext));
  if(j >= 2 && a[i][j-1][0] == next) ans = max(ans, solve(i, j-1, newNext));
  if(j < m && a[i][j+1][0] == next) ans = max(ans, solve(i, j+1, newNext));
  if(next == 1) ans += 1;
  cycle.erase(PII(i, j));
  a[i][j][1] = ans;
  return ans;
}

int main() {
  cin >> n >> m;
  char temp;
  for(int i = 1; i <= n; i ++) {
    for(int j = 1; j <= m; j ++) {
      cin >> temp;
      switch(temp) {
        case 'D' : a[i][j][0] = 1; D.push_back(PII(i, j)); break;
        case 'I' : a[i][j][0] = 2; break;
        case 'M' : a[i][j][0] = 3; break;
        case 'A' : a[i][j][0] = 4; break;
      }
    }
  }
  for(auto p : D) {
    cycle.clear();
    startI = p.first, startJ = p.second;
    ans = max(ans, solve(p.first, p.second, 2));
  }
  if(ans == 0) cout << "Poor Dima!" << endl;
  else if(ans >= n*m) cout << "Poor Inna!" << endl;
  else cout << ans << endl;
  return 0;
}
