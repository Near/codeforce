#include<iostream>
#include<vector>
#include<algorithm>
#include<cstring>
#include<string>
#include<cmath>
using namespace std;

#define FOR(x, m) for(int x = 0; x < m; x ++)
#define MCR(x) (memset(x, 0, sizeof(x)))
typedef pair<int, int> PII;

string s;
int l;
long long res = 1;
int c1 = 0, c2 = 0, c9;

int main() {
  cin >> s;
  l = s.length();
  for(int i = 3; i <= l; i ++) {
    if(s[i-1] == s[i-3] && ((s[i-1] - '0') + (s[i-2] - '0') == 9)) {
      c1 ++;
    } else {
      if(c1 % 2 == 1) res *= (c1 + 3) / 2;
      c1 = 0;
    }
  }
  if(c1 % 2 == 1) res *= (c1 + 3) / 2;
  cout << res << endl;
  return 0;
}
