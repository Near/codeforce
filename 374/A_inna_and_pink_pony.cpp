#include<iostream>
#include<vector>
#include<algorithm>
#include<cstring>
#include<string>
using namespace std;

#define FOR(x, m) for(int x = 0; x < m; x ++)
#define MCR(x) (memset(x, 0, sizeof(x)))
typedef pair<int, int> PII;

const int MAXN = 10001;
int n, m, i, j, a, b;

int main() {
  cin >> n >> m >> i >> j >> a >> b;
  int ansx[] = {-1, -1};
  int ansy[] = {-1, -1};
  int disx[] = {i - 1, n - i};
  int disy[] = {j - 1, m - j};
  int res = -1;
  for(int x = 0; x < 2; x ++) {
    if(disx[x] % a == 0) ansx[x] = disx[x] / a;
  }
  for(int x = 0; x < 2; x ++) {
    if(disy[x] % b == 0) ansy[x] = disy[x] / b;
  }
  for(int x = 0; x < 2; x ++) {
    for(int y = 0; y < 2; y ++) {
      if(abs((ansy[y] - ansx[x]) % 2 != 0)) continue;
      else if(ansx[x] < 0 || ansy[y] < 0) continue;
      else if(ansx[x] == ansy[y] || (ansx[x] > ansy[y] && ansy[y] + ansy[1-y] > 0) || (ansx[x] < ansy[y] && ansx[x] + ansx[1-x] > 0)) {
          if(res == -1) res = max(ansy[y], ansx[x]);
          else res = min(max(ansy[y], ansx[x]), res);
        }
    }
  }
  if(res != -1) cout << res << endl;
  else cout << "Poor Inna and pony!" << endl;
  return 0;
}
