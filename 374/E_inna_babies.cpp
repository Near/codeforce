#include<iostream>
#include<vector>
#include<algorithm>
#include<cstring>
#include<string>
using namespace std;

#define FOR(x, m) for(int x = 0; x < m; x ++)
#define MCR(x) (memset(x, 0, sizeof(x)))
#define PB push_back
typedef pair<int, int> PII;
typedef pair<int, PII> SEG;
vector<PII> blue, red;
vector<SEG> ver, hor;

const int TIMEMAX = 10000000, MAXN = 2000;
vector<int> adj[MAXN];
int n, m, x, y, cnt[2000][2000];

void fix(vector<SEG> &v) {
  sort(v.begin(), v.end());
  int ind = 0;
  for(int i = 0; i < v.size(); i ++) {
    if(ind && v[i].first == v[ind-1].first && v[i].second.first <= v[ind-1].second.second)
      v[ind-1].second.second = v[i].second.second;
    else v[ind ++] = v[i];
  }
  v.resize(ind);
}

bool isIntersect(SEG &v, SEG &h) {
  return v.second.first <= h.first && v.second.second >= h.first && h.second.first <= v.first && h.second.second >= v.first;
}

bool check(int time) {
  ver.clear();
  hor.clear();
  for(auto s : blue) ver.PB(SEG(s.first, PII(s.second - 2*time, s.second + 2*time)));
  for(auto s : red) hor.PB(SEG(s.second, PII(s.first - 2*time, s.first + 2*time)));
  fix(ver);
  fix(hor);

  memset(cnt, 0, sizeof(cnt));

  for(int i = 0; i < ver.size(); i ++) {
    adj[i].clear();
    for(int j = 0; j < hor.size(); j ++) {
      if(isIntersect(ver[i], hor[j])) {
        for(auto k : adj[i]) {
          cnt[k][j] ++;
          if(cnt[k][j] == 2) return true;
        }
        adj[i].PB(j);
      }
    }
  }
  return false;
}

int main() {
  cin >> n >> m;
  FOR(i, n) {
    cin >> x >> y;
    blue.PB(PII(x+y, x-y));
  }
  FOR(i, m) {
    cin >> x >> y;
    red.PB(PII(x+y, x-y));
  }
  int left = 0, right = TIMEMAX;
  while(left < right) {
    int mid = (left + right) / 2;
    if(check(mid)) right = mid;
    else left = mid + 1;
  }
  if(left == TIMEMAX) cout << "Poor Sereja!" << endl;
  else cout << left << endl;
  return 0;
}
