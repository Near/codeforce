from collections import deque
queue = deque()
(x, y) = map(int, raw_input().split())
queue.append((x, y, 0))
status = set([(x, y)])
ans = -1

while len(queue) > 0:
  (x, y, op_num) = queue.popleft()
  if x == y:
    ans = op_num
    break
  for i in [2, 3, 5]:
    if x > y and x%i == 0 and (x/i, y) not in status:
      queue.append((x/i, y, op_num + 1))
      status.add((x/i, y))
    if y > x and y%i == 0 and (x, y/i) not in status:
      queue.append((x, y/i, op_num + 1))
      status.add((x, y/i))

print ans
