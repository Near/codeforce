n = int(raw_input())
vessels = map(int, raw_input().split())
m = int(raw_input())
prev = [0] * n
succ = [0] * n
num = [0] * (n + 1)
for i in range(1, n):
  prev[i] = i-1
  succ[i-1] = i
succ[n-1] = n

for i in range(m):
  query = map(int, raw_input().split())
  if query[0] == 1:
    temp = []
    index = query[1] - 1
    total = num[index] + query[2]
    while total > vessels[index]:
      num[index] = vessels[index]
      total -= vessels[index]
      temp.append(index)
      index = succ[index]
      if index >= n:
        break
      total = total + num[index]
    if index <= n-1:
      num[index] = total
    for j in temp:
      succ[j] = index

  else :
    print num[query[1] - 1]
