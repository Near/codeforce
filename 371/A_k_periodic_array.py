def dist(x, y):
  return len([a for a, b in zip(x, y) if a != b])

n, k = map(int, raw_input().split())
array = map(int, raw_input().split())
temp = [0] * k
count = 0

for i in range(n/k):
  for j in range(k):
    temp[j] += array[i*k+j]

for i in temp:
  count2 = i - n/k
  count1 = n/k - count2
  count += min(count1, count2)

print count
