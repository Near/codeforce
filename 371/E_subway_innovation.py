n = int(raw_input())
x = map(int, raw_input().split())
k = int(raw_input())
origin = {}
for i, e in enumerate(x):
  origin[e] = i
x.sort()
s = [0] * n
for i,e in enumerate(x):
  if i == 0:
    s[i] = x[i]
  else:
    s[i] = s[i-1] + x[i]
bestStart = 0
bestAns = 0
prevAns = bestAns

for i in range(1, n - k + 1):
  diff = (k-1) * (x[i-1] + x[i+k-1]) - 2*(s[i+k-2] - s[i-1])
  prevAns += diff
  if prevAns < bestAns:
    bestAns = prevAns
    bestStart = i
for i in range(bestStart, bestStart + k):
  print origin[x[i]] + 1,
print
