import string
recipe = raw_input()
c = [string.count(recipe, x) for x in ["B", "S", "C"]]
n = map(int, raw_input().split())
p = map(int, raw_input().split())
money = int(raw_input())

n = [x for x,y in zip(n, c) if y > 0]
p = [x for x,y in zip(p, c) if y > 0]
c = [x for x in c if x > 0]

current = min([x/y for x,y in zip(n, c)])
n = [x - current * y for x,y in zip(n, c)]
ans = current

while max(n) > 0:
  cost = 0
  for i, x in enumerate(n):
    if x < c[i]:
      cost += (c[i] - x)*p[i]
  if cost <= money:
    n = [max(x - y, 0) for x,y in zip(n, c)]
    current += 1
    money -= cost
  else:
    break

eachCost = sum([x*y for x,y in zip(c, p)])
current += money / eachCost
print current
