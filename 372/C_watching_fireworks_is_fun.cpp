#include<iostream>
#include<cstring>
#include<cstdio>
#include<cmath>
using namespace std;
const int MAXSIZE=150000;
int n, m, d, a[MAXSIZE+1], cur = 1;
long long b[MAXSIZE+1], t[MAXSIZE+1], ans[2][MAXSIZE+1], p = 0;

int main() {
  cin >> n >> m >> d;
  for(int i = 0; i < m; i ++) {
    cin >> a[i] >> b[i] >> t[i];
    memcpy(ans[cur], ans[1-cur], sizeof(long long) * (n+1));
    for(int j = 1; j <= n; j ++) {
        long long temp = abs(a[i] - j);
        ans[cur][j] += b[i] - temp;
    }
    if(t[i] == p) {
      for(int j = 1; j <= n; j ++) {
        ans[cur][j] = ans[1-cur][j] + b[i] - abs(a[i] - j);
      }
    } else if(p != 0) {
      int dist = (t[i] - p) * d;
      for(int j = 1; j <= n; j ++) {
        for(int k = max(j - dist, 0); k <= min(n, j + dist); k ++) {
          int temp = abs(a[i] - k);
          ans[cur][k] = max(ans[cur][k], ans[1-cur][j] + b[i] - temp);
        }
      }
    }
    p = t[i];
    cur = 1 - cur;
  }
  long long res = ans[1-cur][1];
  for(int i = 1; i <= n; i ++) {
    res = res > ans[1-cur][i] ? res : ans[1-cur][i];
  }
  cout << res << endl;
  return 0;
}
