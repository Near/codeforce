#include<iostream>
#include<algorithm>
using namespace std;
const int MAXSIZE=500000;
int n, s[MAXSIZE+1];
bool vis[MAXSIZE+1];

int main() {
  cin >> n;
  for(int i = 0; i < n; i ++) {
    cin >> s[i];
  }
  sort(s, s+n);
  int c1 = 0, c2 = n/2;
  while(c1 < n/2 && c2 < n) {
    if(s[c1] * 2 <= s[c2]) {
      c1 ++;
      c2 ++;
    } else {
      c2 ++;
    }
  }
  cout << n - c1 << endl;
  return 0;
}
