#include<iostream>
#include<cstdio>
#include<stack>
#include<cstring>
using namespace std;
const int MAXSIZE=40;
int n, m, q, array[MAXSIZE+1][MAXSIZE+1];
int A[MAXSIZE+1], B[MAXSIZE+1], f[MAXSIZE+1][MAXSIZE+1][MAXSIZE+1][MAXSIZE+1];
int d[4][MAXSIZE+2][MAXSIZE+2];

int solution1(int i, int ii, int j, int jj) {
  if(f[i][ii][j][jj] != -1) return f[i][ii][j][jj];
  else if(i == ii && j == jj) return f[i][ii][j][jj] = (array[i][j] == 0);
  int mid = 0, ret = 0;
  if(jj - j < ii - i) {
    mid = (i + ii) >> 1;
    for(int t = 0; t + j <= jj; t ++) {
      A[t] = mid - max(i-1, d[0][mid][t+j]);
      B[t] = min(ii+1, d[2][mid+1][t+j]) - (mid + 1);
      for(int r = t - 1; r >= 0 && A[r] > A[t]; r --) A[r] = A[t];
      for(int r = t - 1; r >= 0 && B[r] > B[t]; r --) B[r] = B[t];
      for(int r = t; r >= 0 && A[r] > 0 && B[r] > 0; r --) ret += A[r] * B[r];
    }
    ret += solution1(i, mid, j, jj) + solution1(mid+1, ii, j, jj);
  } else {
    mid = (j + jj) >> 1;
    for(int t = 0; t + i <= ii; t ++) {
      A[t] = mid - max(j-1, d[1][t+i][mid]);
      B[t] = min(jj+1, d[3][t+i][mid+1]) - mid - 1;
      for(int r = t - 1; r >= 0 && A[r] > A[t]; r --) A[r] = A[t];
      for(int r = t - 1; r >= 0 && B[r] > B[t]; r --) B[r] = B[t];
      for(int r = t; r >= 0 && A[r] > 0 && B[r] > 0; r --) ret += A[r] * B[r];
    }
    ret += solution1(i, ii, j, mid) + solution1(i, ii, mid+1, jj);
  }
  f[i][ii][j][jj] = ret;
  return ret;
}

int main() {
  string s;
  cin >> n >> m >> q;
  for(int i = 1; i <= n; i ++) {
    cin >> s;
    for(int j = 1; j <= m; j ++) {
      array[i][j] = s[j-1] - '0';
    }
  }
  for(int i = 1; i <= n; i ++) {
    for(int j = 1; j <= m; j ++) {
      d[0][i][j] = array[i][j] == 0 ? d[0][i-1][j] : i;
      d[1][i][j] = array[i][j] == 0 ? d[1][i][j-1] : j;
    }
  }
  for(int i = n; i >= 1; i --) {
    for(int j = m; j >= 1; j --) {
      d[2][i][j] = array[i][j] == 0 ? ((i == n) ? n + 1 : d[2][i+1][j]) : i;
      d[3][i][j] = array[i][j] == 0 ? ((j == m) ? m + 1 : d[3][i][j+1]) : j;
    }
  }
  memset(f, -1, sizeof(f));
  int a, b, c, d;
  for(int i = 0; i < q; i ++) {
    cin >> a >> b >> c >> d;
    cout << solution1(a, c, b, d) << endl;
  }
  return 0;
}
