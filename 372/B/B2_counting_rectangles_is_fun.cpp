#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
const int MAXSIZE=40;
int n, m, q, array[MAXSIZE+1][MAXSIZE+1];
int f[MAXSIZE+1][MAXSIZE+1][MAXSIZE+1][MAXSIZE+1];
int p[MAXSIZE+1][MAXSIZE+1];

int main() {
  string s;
  cin >> n >> m >> q;
  for(int i = 1; i <= n; i ++) {
    cin >> s;
    for(int j = 1; j <= m; j ++) {
      array[i][j] = s[j-1] - '0';
      p[i][j] = array[i][j] == 1 ? j : p[i][j-1];
    }
  }
  for(int i = 1; i <= m; i ++) {
    for(int j = 1; j <= n; j ++) {
      for(int x = i; x <= m; x ++) {
        for(int y = j; y <= n; y ++) {
          f[i][j][x][y] = f[i][j][x-1][y] + f[i][j][x][y-1] - f[i][j][x-1][y-1];
          int now = j - 1;
          for(int k = x; k >= i; k --) {
            now = max(now, p[k][y]);
            f[i][j][x][y] += y - now;
          }
        }
      }
    }
  }
  int a, b, c, d;
  for(int i = 0; i < q; i ++) {
    cin >> a >> b >> c >> d;
    cout << f[a][b][c][d] << endl;
  }
  return 0;
}
