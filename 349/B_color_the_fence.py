v = int(raw_input())
cost = map(int, raw_input().split())
mcost = min(cost)
mdigit = 0
d = v/mcost
ans = ""

if d <= 0:
  ans = -1

for i in range(9, 0, -1):
  if cost[i-1] == mcost:
    mdigit = i
    break

while d > 0:
  if v == d * mcost:
    ans = ans + str(mdigit) * d
    break
  d -= 1
  for i in range(9, 0, -1):
    if v - cost[i-1] >= d * mcost :
      v -= cost[i-1]
      ans = ans + str(i)


print ans
