#include<iostream>
using namespace std;

int main() {
  int n, a[1000];
  cin >> n;
  for(int i = 0; i < n; i ++) cin >> a[i];
  int m, c, i;
  m = max(a[0], a[1]);
  c = a[0] + a[1] - m;
  i = a[0] > a[1] ? 0 : 1;
  for(int j = 2; j < n; j ++) {
    if(a[j] > m) {
      c = m;
      m = a[j];
      i = j;
    } else if(a[j] > c) {
      c = a[j];
    }
  }
  cout << i + 1 << " " << c << endl;
  return 0;
}
