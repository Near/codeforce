#include<iostream>
#include<algorithm>
using namespace std;

int main() {
  int l = 0, r = 0, n, t[1000], T, ans = 1;
  cin >> n;
  for(int i = 0; i < n; i ++) cin >> t[i];
  cin >> T;
  sort(t, t + n);
  while(r < n) {
    if(t[r] - t[l] > T) {
      ans = max(ans, r - l);
      do {
        l ++;
      } while(t[r] - t[l] > T);
    }
    r ++;
  }
  ans = max(r - l, ans);
  cout << ans << endl;
  return 0;
}
