#include<iostream>
#include<vector>
#include<queue>
#include<map>
#include<cstdlib>
#include<string>
#include<algorithm>
using namespace std;

const int MAXN = 100000, MAXM = 10000;
long long n = 10000, m = 10000, a[MAXN][2];

int main() {
  cin >> n;
  long long ans = 0;
  for(int i = 0; i < n; i ++)
      cin >> a[i][0];
  for(int i = 0; i < n; i ++)
      cin >> a[i][1];

  for(int i = 0; i < n; i ++) {
    if(a[i][0] * 2 < a[i][1] || a[i][1] <= 1) ans --;
    else {
      long long x = a[i][1] / 2, y = a[i][1] - x;
      ans += x * y;
    }
  }

  cout << ans << endl;

  return 0;
}
