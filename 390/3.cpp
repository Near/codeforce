#include<iostream>
#include<vector>
#include<queue>
#include<map>
#include<cstdlib>
#include<string>
#include<algorithm>
using namespace std;

const int MAXN = 100000, MAXW = 100000, MAXK = 10;
int n, w, k, a[MAXN+1], q[MAXW][2], sum[MAXN+1], to[MAXN+1];

int main() {
  cin >> n >> k >> w;
  char c;
  sum[0] = to[0] = 0;
  for(int i = 1; i <= n; i ++) {
    cin >> c;
    if(c == '1') {
      a[i] = 1; 
    }
    if(i <= k) sum[i] = a[i];
    else sum[i] = sum[i-k] + a[i];
    to[i] = to[i-1] + a[i];
  }

  int l, r;
  for(int i = 0; i < w; i ++) {
    cin >> l >> r;
    int temp = sum[r] - sum[l - 1], total = 0, temp2 = to[r] - to[l-1];
    total += (r - l + 1) / k - temp;
    total += temp2 - temp;

    cout << total << endl;
  }

  return 0;
}
