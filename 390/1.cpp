#include<iostream>
#include<vector>
#include<queue>
#include<map>
#include<cstdlib>
#include<string>
#include<algorithm>
using namespace std;

const int MAXN = 100000, MAXM = 10000;
int n = 10000, m = 10000, a, b;

int main() {
  int x[101], y[101], cx = 0, cy = 0;
  cin >> n;
  for(int i = 0; i < 101; i ++)
    x[i] = y[i] = 0;
  for(int i = 0; i < n; i ++) {
    cin >> a >> b;
    if(x[a] == 0) { cx ++; x[a] ++; }
    if(y[b] == 0) { cy ++; y[b] ++; }
  }

  cout << (cx < cy ? cx : cy) << endl;

  return 0;
}
