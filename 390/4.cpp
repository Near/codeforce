#include<iostream>
#include<vector>
#include<queue>
#include<map>
#include<cstdlib>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

const int MAXN = 50, MAXM = 50, MAXK = 2500;
int n, m, k, c[101];
char path[51][51][1000];
vector<char*> res;

int main() {
  cin >> n >> m >> k;
  strcpy(path[1][1], "(1, 1)");
  res.push_back(path[1][1]);
  int temp = k - 1, len = 0, ans = 1;
  for(int len = 2; len <= n + m && temp > 0; len ++) {
    for(int i = max(1, len - m + 1); i <= min(n, len) && temp > 0; i ++) {
      if(i >= 2) sprintf(path[i][len - i + 1], "%s (%d, %d)", path[i - 1][len - i + 1], i, len - i + 1);
      else sprintf(path[i][len - i + 1], "%s (%d, %d)", path[i][len - i], i, len - i + 1);
      res.push_back(path[i][len-i+1]);
      temp -= 1;
      ans += len;
    }
  }

  cout << ans << endl;

  while(!res.empty()) {
    printf("%s\n", res.back());
    res.pop_back();
  }

  return 0;
}
