#include<iostream>
#include<vector>
#include<queue>
#include<map>
#include<cstdlib>
#include<string>
#include<algorithm>
using namespace std;

const int MAXN = 4000000, MAXM = 4000000;
int n, m, w, sum[MAXN+1][MAXM+1];

int getRect(int x1, int y1, int x2, int y2) {
  return sum[x2][y2] - sum[x1-1][y2] - sum[x2][y1-1] + sum[x1-1][y1-1];
}

int main() {
  cin >> n >> m >> w;
  for(int i = 0; i < n; i ++)
    for(int j = 0; j < m; j ++)
      sum[i][j] = 0;

  int c, x1, y1, x2, y2, v;
  for(int i = 0; i < w; i ++) {
    cin >> c >> x1 >> y1 >> x2 >> y2;
    if(c == 0) {
      cin >> v;
      for(int i = x1; i <= n; i ++)
        for(int j = y1; j <= m; j ++)
          sum[i][j] += min(i - x1 + 1, x2 - x1 + 1) * min(j - y1 + 1, y2 - y1 + 1) * v;
    } else {
      cout << getRect(x1, y1, x2, y2) - getRect(1, 1, x1-1, y1-1) - getRect(1, y2+1, x1-1, m)
        - getRect(x2+1, 1, n, y1-1) - getRect(x2+1, y2+1, n, m) << endl;
    }
  }
  return 0;
}
