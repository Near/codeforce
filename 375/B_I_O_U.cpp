#include<iostream>
#include<cstring>
#include<algorithm>
#include<vector>
#include<map>
#include<queue>
#include<set>
#include<cstdlib>
using namespace std;

typedef pair<int, int> PII;
const int MAXN = 10000;
int n, m, s, t, w, in[101], out[101], total = 0;

int main() {
  cin >> n >> m;
  for(int i = 0; i < m; i ++) {
    cin >> s >> t >> w;
    out[s] += w;
    in[t] += w;
  }
  
  for(int i = 1; i <= n; i ++) {
    if(in[i] > out[i]) continue;
    else total += (out[i] - in[i]);
  }
  cout << total << endl;
  return 0;
}
