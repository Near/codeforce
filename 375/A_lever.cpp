#include<iostream>
#include<cstring>
#include<algorithm>
#include<vector>
#include<map>
#include<queue>
#include<set>
#include<cstdlib>
using namespace std;

const int MAXN = 1000000;
long long a[MAXN+1];
string s;

int main() {
  cin >> s;
  long long l = s.length(), p = 0;
  long long sum = 0;
  for(int i = 0; i < l; i ++) {
    a[i] = 0;
    if(s[i] == '=') continue;
    else if(s[i] == '^') p = i;
    else a[i] = s[i] - '0';
  }
  for(int i = 0; i < l; i ++)
    if(i < p) sum += a[i] * (p - i);
    else sum -= a[i] * (i - p);
  if(sum < 0) cout << "right" << endl;
  else if(sum == 0) cout << "balance" << endl;
  else cout << "left" << endl;
  return 0;
}
