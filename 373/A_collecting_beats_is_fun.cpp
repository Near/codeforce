#include<iostream>
#include<string>
using namespace std;
const int MAXSIZE=10000;
int k, count[9];
string s;

int main() {
  cin >> k;
  for(int i = 0; i < 4; i ++) {
    cin >> s;
    for(int j = 0; j < 4; j ++) {
      if(s[j] == '.') continue;
      else {
        int index = s[j] - '1';
        count[index] ++;
        if(count[index] > 2*k) {
          cout << "NO" << endl;
          return 0;
        }
      }
    }
  }
  cout << "YES" << endl;
  return 0;
}
