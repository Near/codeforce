#include<iostream>
using namespace std;
const int MAXSIZE=10000;
long long w, m, k;

long long digits(long long x) {
  int c = 0;
  while(x > 0) {
    x /= 10;
    c ++;
  }
  return c;
}

long long pow_10(long long digits) {
  long long ans = 1;
  while(digits > 1) {
    ans *= 10;
    digits --;
  }
  return ans;
}

int main() {
  cin >> w >> m >> k;
  long long  d = digits(m), ans = 0, up = pow_10(d+1), low = m;
  while(true) {
    long long temp = w / (d * k);
    if(temp <= up - low) {
      ans += temp;
      cout << ans << endl;
      return 0;
    } else {
      w = w - (up - low) * d * k;
      ans += up - low;
      low = up;
      up *= 10;
      d += 1;
    }
  }
  return 0;
}
